<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require './Exception.php';
require './PHPMailer.php';
require './SMTP.php';

$mail = new PHPMailer(true);
$mail_recipient = new PHPMailer(true);
$data = $_POST;

$bedrijf          = $data[bedrijf];
$voornaam         = $data[voornaam];
$achternaam       = $data[achternaam];
$adres            = $data[adres];
$postcode         = $data[postcode];
$plaats           = $data[plaats];
$email            = $data[email];
$telefoonnummer   = $data[telefoonnummer];
$datum            = $data[datum];
$personen         = $data[personen];

$email_confirm = '<!doctype html>
<html>

<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
  <table cellspacing="0" cellpadding="0"
  style="border-collapse: collapse; background-color: #efeff0; font-family: "Barlow", sans-serif; max-width: 600px; margin: 0 auto; border: none; border: 0;"
  bgcolor="#e6e6e7" width="600">
    <tr>
    <td></td>
      <td width="600">
        <img src="http://afspraak.deanone.nl/img/email-bevestiging-header.jpg" style="width: 100%; max-width: 600px; width:600px;" alt="Bedankt">
      </td>
      <td></td>
    </tr>
    <tr class="main">
    <td></td>
      <td style="padding: 40px 20px; max-width:600px; box-sizing: border-box;" width="600">';
$email_confirm .= '<p>Beste ' . $voornaam . ',</p>';
$email_confirm .= '<p>
Dank voor het aanvragen van een lunchafspraak. We nemen binnen 2 werkdagen contact met je op om een datum te prikken.
</p>
<p>
  Tot snel,
</p>
<p>
<b>Mano Joren</b> <br>
<span class="uc" style="text-transform: uppercase;">business development manager Dean One</span>
<div class="stroke" style="width: 30px; height: 2px; background-color: #d70019;"></div>
</p>
</td>
<td></td>
</tr>
</table>
</body>

</html>';

$email_data = '<!doctype html>
<html>

<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
<table cellspacing="0" cellpadding="0"
style="border-collapse: collapse; background-color: #efeff0; font-family: "Barlow", sans-serif; max-width: 600px; margin: 0 auto; border: none; border: 0;"
bgcolor="#e6e6e7" width="600">
<tr>
<td></td>
  <td width="600">
    <img src="http://afspraak.deanone.nl/img/email-nieuwe-afspraak-header.jpg" style="width: 100%; max-width: 600px; width:600px;" alt="Nieuwe afspraak">
  </td>
  <td></td>
</tr>
<tr class="main">
<td></td>
  <td style="padding: 40px 20px; max-width:600px box-sizing: border-box;" width="600">';
$email_data .= '<p>Er is een nieuwe afspraak gemaakt door ' . $voornaam . ' voor ' . $personen . ' personen.</p>';
$email_data .= '<p>
Bedrijf: '. $bedrijf .'<br>
Voornaam: '. $voornaam .'<br>
Achternaam: '. $achternaam .'<br>
Adres: '. $adres .'<br>
Postcode: '. $postcode .'<br>
Plaats: '. $plaats .'<br>
Email: '. $email .'<br>
Telefoonnummer: '. $telefoonnummer .'<br>
Aantal personen: '. $personen .'
</p>';
$email_data .= '</td>
<td></td>
</tr>
</table>


</body>

</html>';



try {
  $mail->isSMTP();
  $mail->Host = 'smtp-infra.deanone.nl';
  $mail->SMTPAuth = false;
  $mail->SMTPOptions = array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
      )
    );
  $mail->Port = 25;

  $mail->setFrom('mano.joren@deanone.nl', "Mano Joren");
  $mail->addAddress("mano.joren@deanone.nl");
  $mail->addBcc("marketing@deanone.nl");
  $mail->addReplyTo('mano.joren@deanone.nl', "Mano Joren");

  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = 'Er is een afspraakverzoek';
  $mail->Body    = $email_data;

  $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
  echo 
  'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

try {
  $mail_recipient->isSMTP();
  $mail_recipient->Host = 'smtp-infra.deanone.nl';
  $mail_recipient->SMTPAuth = false;
  $mail_recipient->SMTPOptions = array(
    'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
      )
    );
  $mail_recipient->Port = 25;


  $mail_recipient->setFrom('mano.joren@deanone.nl', "Mano Joren");
  $mail_recipient->addAddress($email);   // Add a recipient
  $mail_recipient->addReplyTo('mano.joren@deanone.nl', 'mano.joren@deanone.nl');

  $mail_recipient->isHTML(true);                                  // Set email format to HTML
  $mail_recipient->Subject = 'Bedankt voor je aanvraag ' . $voornaam;
  $mail_recipient->Body    = $email_confirm;

  $mail_recipient->send();
    echo 'Message has been sent';
} catch (Exception $e) {
  echo 
  'Message could not be sent. Mailer Error: ', $mail_recipient->ErrorInfo;
}


?>