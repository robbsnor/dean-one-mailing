import "../styles/all.scss";

import $ from "jquery";
import "bootstrap";
import "@fortawesome/fontawesome-free/js/all.min.js";
import "@chenfengyuan/datepicker";
import url from "url-parameters";

// Example starter JavaScript for disabling form submissions if there are invalid fields

$(document).ready(function() {
  const form = $("#form");
  const urlParams = url.getParams();
  const len = $.map(urlParams, function(n, i) {
    return i;
  }).length;

  if (len > 0) {
    $.each(urlParams, function(key, value) {
      var reg = /^[\w &%.\-_]+$/;
      if (!reg.test(value)) {
        var newValue = "Input niet geldig";
      } else {
        var newValue = value.replace("%26", "&");
      }
      form.find("input").val(function(index, elementVal) {
        if (this.id == key) {
          if (this.id === "voornaam") {
            $("#title-name").html(newValue + "?");
          }
          return newValue;
        } else {
          return elementVal;
        }
      });
    });
  } else {
    $("#title-name").remove();
    $("#title-variable").html("groeien?<div class='mb-t'</div>");
    form.find("fieldset").removeAttr("disabled");
  }
});

var disabledDates = ["25/12/2019", "26/12/2019", "3/1/2020"];

var datePicker = $('[data-toggle="datepicker"]').datepicker({
  language: "nl-NL",
  format: "dd-mm-yyyy",
  weekStart: 1,

  trigger: ".x",
  autoHide: true,

  filter: function(date, view) {
    if (
      (date.getDay() === 0 && view === "day") ||
      (date.getDay() === 6 && view === "day")
    ) {
      return false;
    }

    // var thisDate =
    //   date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    // if ($.inArray(thisDate, disabledDates) == -1) {
    //   return false;
    // }
    // for (var i = 0; i < disabledDates.length; i++) {
    //   var month = datePicker.datepicker("getMonthName");
    //   if (
    //     disabledDates[i].month === month &&
    //     disabledDates[i].day === date.getDay()
    //   ) {
    //     return false;
    //   }
    // }
  }
});

form.addEventListener(
  "submit",
  function(event) {
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      event.preventDefault();
      $(".loader").addClass("show");
      $("form button").remove();

      let formData = getFormData(form);

      $.post("./php/mail.php", formData)
        .done(function(callback, status, data) {
          $(".loader").removeClass("show");
          if (status === "success") {
            $(".message").html(
              "Je aanvraag is verstuurd! <br>Je ontvangt een bevestiging per mail."
            );
          }
          $(form).remove();
        })
        .fail(function() {
          console.log("error");
        });
    }
    form.classList.add("was-validated");
  },
  false
);

function getFormData(form) {
  var data = {};
  var formInputs = $(form).find("input");
  $.each(formInputs, function(index, val) {
    data[val.id] = val.value;
  });
  data["personen"] = $("#personen").val();
  return data;
}
