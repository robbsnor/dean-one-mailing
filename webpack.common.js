const merge = require('webpack-merge');
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: './src/js/index.js'
  },
  module: {
  	rules: [ 
      {
        test: /\.html$/,
        use: ['html-loader']
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            // name: '[name].[hash].[ext]',
            name: '[name].[ext]',
            outputPath: 'img'
          }
        }
      }
  	]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/templates/index.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'email-bevestiging.html',
      template: 'src/templates/email-bevestiging.html'
    }),
    new HtmlWebpackPlugin({
      filename: 'email-nieuwe-afspraak.html',
      template: 'src/templates/email-nieuwe-afspraak.html'
    })
  ],
  devtool: 'inline-source-map',
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
};