const gulp = require("gulp");

function defaultTask() {
  return gulp.src("./src/php/*.php").pipe(gulp.dest("dist/php"));
}

exports.default = defaultTask;
