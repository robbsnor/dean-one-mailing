## Install dependencies
```
npm i
```

## Run dev
```
npm run start
```

## Run production
```
npm run build
```

## Url
```
http://192.168.2.146:8080/?voornaam=Patrick&achternaam=Boer&bedrijf=Studio%20DIN&adres=Maassluisstraat%202&postcode=1062%20GD&plaats=Amsterdam
```
